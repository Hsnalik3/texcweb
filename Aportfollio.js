$(document).ready(function(){
  SubmittedUserGrid();
});

var SubmittedUserGrid = () => {
  $.ajax({
   url: 'http://localhost:9890/api/startups',
   error: onerrorr,
   contentType: 'application/x-www-form-urlencoded',
   success: function(data) {

      $('#startupDetails').empty();
      for (var i = 0; i < data.length ; i++) {
        var active = (data[i].IsActive == 1) ? "True":"False";
        $('#startupDetails').append('<tr><td>&nbsp;&nbsp;'+data[i].Name+'</td>'
            +'<td>&nbsp;&nbsp;'+data[i].WebLink+'</td>'
            +'<td>&nbsp;&nbsp;'+data[i].InvestmentType+'</td>'
            +'<td>&nbsp;&nbsp;'+active+'</td>'
            +'<td>&nbsp;&nbsp;<span class="fa fa-pencil" style="color:blue;cursor:pointer" onclick="(\'' + data[i].Id + '\')" title="Edit Data" style="cursor:pointer"></span></td></tr>'
          );
      }
      $('#startuptable').DataTable();
   },
   type: 'GET'
});
}

var onerrorr = (resp)=> {
    swal("Something went wrong !", {
      icon: "warning",
    });
};

var InsertData = ()=>{
    let name = $('#txtsname').val();
    let link = $('#txtweblink').val();
    let sdesc = $('#txtsdesc').val();
    let ldesc = $('#txtldesc').val();
    let type = $('#ddlit').val();
    let isactive = $('#chactive').val();

    var filedata = new FormData();
    $.each($('#newfile')[0].files, function(i, file) {
      filedata.append('docFile', file);
    });

}
