// Plugin: jQuery.scrollSpeed
// Source: github.`com/nathco/jQuery.scrollSpeed
// Author: Nathan Rutzky
// Update: 1.0.2
(function($) {

    jQuery.scrollSpeed = function(step, speed, easing) {

        var $document = $(document),
            $window = $(window),
            $arrow = $("#arrow"),
            $arrow1 = $("#arrow1"),
            $body = $('.row'),
            option = easing || 'default',
            root = 0,
            scroll = false,
            scrollY,
            scrollX,
            view;
             var width = $("#width").width();

        if (window.navigator.msPointerEnabled)
            return false;

        var swipe = new Hammer(document.getElementsByTagName("BODY")[0]);
        // detect swipe and call to a function
        swipe.on('swiperight swipeleft', function(e) {
          e.preventDefault();
          if (e.type == 'swipeleft') {
            var deltaY = undefined,
                 detail = 1;
            scrollY = $document.height() > $window.height();
            scrollX = $document.width() > $window.width();
            scroll = true;

            if (scrollX) {
                view = $window.width();
                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.width() + $document.width() + $document.width() + $document.width()  ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;
                $body.stop().animate({
                    scrollLeft: root 
                }, speed, option, function() {
                    scroll = false;
                });
            }
            if (scrollY && !scroll) root = $window.scrollTop();
            if (scrollX && !scroll) root = $window.scrollLeft();
          } 
          else {
            var deltaY = undefined,
            detail = 1;
            scrollY = $document.height() > $window.height();
            scrollX = $document.width() > $window.width();
            scroll = true;

            if (scrollX) {
                view = $window.width();
                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.width() + $document.width() + $document.width()  ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;
                $body.stop().animate({
                    scrollLeft: root -= step + view - 145
                }, speed, option, function() {
                    scroll = false;
                });
            }
          }

        });
        $arrow.on('click',function(e){
            var deltaY = e.originalEvent.wheelDeltaY,
            detail = e.originalEvent.detail;
            scrollY = $document.height() >= $window.height();
            scrollX = $document.width() >= $window.width();
            scroll = true;

            if (scrollX) {
                view = $window.width();
                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.width() + $document.width() + $document.width() + $document.width()  ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;
                $body.stop().animate({
                    scrollLeft: root 
                }, speed, option, function() {
                    scroll = false;
                });
            }
            if (scrollY && !scroll) root = $window.scrollTop();
            if (scrollX && !scroll) root = $window.scrollLeft();
        }); 
        $arrow1.on('click',function(e){
            var deltaY = e.originalEvent.wheelDeltaY,
            detail = e.originalEvent.detail;
            scrollY = $document.height() >= $window.height();
            scrollX = $document.width() >= $window.width();
            scroll = true;

            if (scrollX) {
                view = $window.width();
                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.width() + $document.width() + $document.width()  ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;
                $body.stop().animate({
                    scrollLeft: root -= step + view - 145
                }, speed, option, function() {
                    scroll = false;
                });
            }
        });
        $window.on('mousewheel DOMMouseScroll', function(e) {

            var deltaY = e.originalEvent.wheelDeltaY,
                detail = e.originalEvent.detail;
            scrollX = $document.height() >= $window.height();
            scrollX = $document.width() >= $window.width();

            scroll = true;
            if (scrollX) {
                view = $window.width();
                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.width() + $document.width() + $document.width() + $document.width()  ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;

                $body.stop().animate({
                    scrollLeft: root 
                }, speed, option, function() {
                    scroll = false;
                }) 
                
            }

                if (root == step) {
                    $("#two").addClass('active1')
                    $("#one").removeClass('active1')
                    $("#three").removeClass('active1')
                }
                if (root > step ) {
                    $("#two").removeClass('active1')
                    $("#three").addClass('active1')
                }
                if (root == 0 ) {
                    $("#two").removeClass('active1')
                    $("#one").addClass('active1')
                }     

            return false;


        }).on('scroll', function() {

            if (scrollY && !scroll) root = $window.scrollTop();
            if (scrollX && !scroll) root = $window.scrollLeft();

        }).on('resize', function() {

            if (scrollY && !scroll) view = $window.height();
            if (scrollX && !scroll) view = $window.width();

        });
    };

    jQuery.easing.default = function(x, t, b, c, d) {
        return c * Math.sin(t/d * (Math.PI/2)) + b;
    };

})(jQuery);