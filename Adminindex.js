$(document).ready(function(){
  if ($.cookie('Auth') != 'Admin') {
    window.location.href = "login.html";
  }
  SubmittedUserGrid();
});

var SubmittedUserGrid = () => {
  $.ajax({
   url: 'https://nodetenxcservice-exyousqoon.now.sh/api/user',
   error: onerrorr,
   contentType: 'application/x-www-form-urlencoded',
   success: function(data) {

      $('#surveyUsr').empty();
      for (var i = 0; i < data.length ; i++) {
        let btnaction = '';
        if (data[i].Status == 'Not Seen') {
          btnaction = '<td> <span class="fa fa-file-text-o" onclick="showpitch(\'' + data[i].ShortPitch + '\')" title="view short pitch" style="cursor:pointer"></span>&nbsp;&nbsp;<a class="fa fa-download" style="cursor:pointer" target="_blank" href="'+data[i].DocumentPath+'" title="Download the ppt file"></a>&nbsp;&nbsp;<span class="fa fa-check-square-o" onclick="responseUser(\'' + data[i].Email + '\','+data[i].Id+',\'' + data[i].CompanyName + '\')" style="cursor:pointer" title="accept or reject the application"></span> </td></tr>'
        }else {
          btnaction = '<td>&nbsp;&nbsp;&nbsp; <span class="fa fa-file-text-o" onclick="showpitch(\'' + data[i].ShortPitch + '\')" title="view short pitch" style="cursor:pointer"></span>&nbsp;&nbsp;<a class="fa fa-download" style="cursor:pointer"target="_blank" href="'+data[i].DocumentPath+'" title="Download the ppt file"></a> </td></tr>'
        }
        $('#surveyUsr').append('<tr><td>'+data[i].CompanyName+'</td>'
            +'<td>'+data[i].UserName+'</td>'
            +'<td>'+data[i].Email+'</td>'
            +'<td>'+data[i].StartupSite+'</td>'
            +'<td>'+data[i].BestApplysTo+'</td>'
            +'<td>'+data[i].Name+'</td>'
            +'<td>'+data[i].Status+'</td>'
            +'<td>'+data[i].City+'</td>'
            + btnaction
          );
      }
      $('#usrtable').DataTable();

   },
   //dataType: 'jsonp',
   type: 'GET'
});
}

var showpitch = (msg)=> {
  swal("Short Pitch", msg);
}

var responseUser = (email,iid,startname)=> {
  swal({
  title: "Application Response",
  text: "Accept or Reject the application",
  icon: "warning",
  buttons: ["Reject", "Accept"],
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    $.ajax({
     url: 'https://nodetenxcservice-exyousqoon.now.sh/api/usrresponse',
     error: onerrorr,
     contentType: 'application/json',
     success: onresponseSuccess,
     type: 'POST',
     dataType:'json',
     data: '{"response":"yes","Email":"'+email+'","Id":'+iid+',"startname":"'+startname+'"}'
   });

    swal("Application Accepted", {
      icon: "success",
    });
  } else {
    $.ajax({
     url: 'https://nodetenxcservice-exyousqoon.now.sh/api/usrresponse',
     error: onerrorr,
     contentType: 'application/json',
     success: onresponseSuccess,
     type: 'POST',
     dataType:'json',
     data: '{"response":"no","Email":"'+email+'","Id":'+iid+',"startname":"'+startname+'"}'
   });
    swal("Application Rejected", {
      icon: "success",
    });
  }
});
}

var onerrorr = (resp)=> {
  if (resp.status == 200) {
      swal("Respose successfully send !!", {
        icon: "success",
      });
      SubmittedUserGrid();
  }else {
    swal("Something went wrong !", {
      icon: "error",
    });
  }
};
var onresponseSuccess = (response)=>{
  swal("Respose successfully send !!", {
    icon: "success",
  });
console.log('updated')
  SubmittedUserGrid();
}
