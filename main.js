(function($){
  var header1 = $('#section1');
  var header1img = $('#section1img'); 
  var heading1h1 = $('#sectionheading1'); 
  var heading1h2 = $('#sectionheading2'); 

  var header2 = $('#section2');
  var header2img = $('#section2img');
  var heading1h3 = $('#sectionheading3'); 
  var heading1h4 = $('#sectionheading4');   

  var header3 = $('#section3');
  var header3img = $('#section3img');
  var heading1h4 = $('#sectionheading4'); 
  var heading1h5 = $('#sectionheading5'); 

  var header4 = $('#section4');
  var header5 = $('#section5');
  var header6 = $('#section6');


  header1.hover(function() {
    TweenLite.to(header1img, .5, {opacity:1,y:50,zIndex:0});
    TweenLite.to(header2img, .5, {opacity:0,y:0});
    TweenLite.to(header3img, .5, {opacity:0,y:0});

    TweenLite.to(header2, 1, {background:'#ffffff'});
    TweenLite.to(header2, 1, {background:'#ffffff',opacity:0.8});
  }, function() {
    TweenLite.to(header1img, .5, {opacity:1,y:50,zIndex:0});
  });
  
  header2.hover(function() {
    TweenLite.to(header1img, .5, {opacity:0,y:0});
    TweenLite.to(header3img, .5, {opacity:0,y:0});
    TweenLite.to(header2img, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header1, 1, {background:'#ffffff',opacity:0.8});
    TweenLite.to(header3, 1, {background:'#ffffff',opacity:0.8});
  }, function() {
    TweenLite.to(header2img, .5, {opacity:1,y:50,zIndex:0});
  }); 
  header3.hover(function() {
    TweenLite.to(header1img, .5, {opacity:0,y:0});
    TweenLite.to(header2img, .5, {opacity:0,y:0});
    TweenLite.to(header3img, .5, {opacity:1,y:50,zIndex: 1});
    TweenLite.to(header3, 1, {background:'#ffffff',opacity:0.8});
  }, function() {
    TweenLite.to(header3img, .5, {opacity:1,y:50,zIndex:0});
    TweenLite.to(header3, 1, {background:'#ffffff',opacity:0.8}); 
  });
  header4.hover(function() {
    TweenLite.to(header3img, .5, {opacity:0,y:0});
  }, function() {
    
  }); header5.hover(function() {
    TweenLite.to(header3img, .5, {opacity:0,y:0});
  }, function() {
    
  }); header6.hover(function() {
    TweenLite.to(header3img, .5, {opacity:0,y:0});
  }, function() {
    
  });

// slick
  
var header11 = $('#section11');
  var header1img1 = $('#section1img1'); 

  var header21 = $('#section21');
  var header2img1 = $('#section2img1'); 

  var header31 = $('#section31');
  var header3img1 = $('#section3img1');

  var header41 = $('#section41');
  var header4img1 = $('#section4img1');

  var header51 = $('#section51');
  var header5img1 = $('#section5img1');

  var header61 = $('#section61');
  var header6img1 = $('#section6img1');  

  var header71 = $('#section71');
  var header7img1 = $('#section7img1');

  var header81 = $('#section81');
  var header8img1 = $('#section8img1');

  var header91 = $('#section91');
  var header9img1 = $('#section9img1');

    TweenLite.to(header1img1, .1, {opacity:0.4});
    TweenLite.to(header2img1, .1, {opacity:0.4});
    TweenLite.to(header3img1, .1, {opacity:0.4});
    TweenLite.to(header21, 1, {background:'#ffffff'});
    TweenLite.to(header31, 1, {background:'#ffffff'});

    TweenLite.to("#rect1",1, {width:400});
    TweenLite.to("#rect2",1, {width:400});
  header11.hover(function() {
    TweenLite.to(header1img1, .5, {opacity:1,y:50,zIndex:0});
    TweenLite.to(header2img1, .5, {opacity:0.4,y:0});
    TweenLite.to(header3img1, .5, {opacity:0.4,y:0});

    TweenLite.to(header21, 1, {background:'#ffffff'});
    TweenLite.to(header21, 1, {background:'#ffffff',opacity:0.8});
  }, function() {
    TweenLite.to(header1img1, .5, {opacity:1,y:50,zIndex:0});
  });
  
  header21.hover(function() {
    TweenLite.to(header1img1, .5, {opacity:.4,y:0});
    TweenLite.to(header3img1, .5, {opacity:.4,y:0});
    TweenLite.to(header2img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header11, 1, {background:'#ffffff',opacity:0.8});
    TweenLite.to(header31, 1, {background:'#ffffff',opacity:0.8});
  }, function() {
    TweenLite.to(header2img1, .5, {opacity:1,y:50,zIndex:0});
  }); 
  header31.hover(function() {
    TweenLite.to(header1img1, .5, {opacity:.4,y:0});
    TweenLite.to(header2img1, .5, {opacity:.4,y:0});
    TweenLite.to(header3img1, .5, {opacity:1,y:50,zIndex: 1});
    TweenLite.to(header31, 1, {background:'#ffffff',opacity:0.8});
  }, function() {
    TweenLite.to(header3img1, .5, {opacity:1,y:50,zIndex:0});
    TweenLite.to(header31, 1, {background:'#ffffff',opacity:0.8}); 
  });
  header41.hover(function() {
    TweenLite.to(header1img1, .5, {opacity:0,y:0});
    TweenLite.to(header2img1, .5, {opacity:0,y:0});
    TweenLite.to(header3img1, .5, {opacity:0,y:0});
    TweenLite.to(header4img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header51, 1, {background:'#ffffff',opacity:0.8}); 
  }, function() {
    TweenLite.to(header4img1, .5, {opacity:0,y:0});
    TweenLite.to(header51, 1, {background:'#ffffff',opacity:1}); 

  }); header51.hover(function() {
    TweenLite.to(header3img1, .5, {opacity:0,y:0});
    TweenLite.to(header4img1, .5, {opacity:0,y:0});
    TweenLite.to(header5img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header61, 1, {background:'#ffffff',opacity:0.8}); 

  }, function() {
    TweenLite.to(header5img1, .5, {opacity:0,y:0});
    TweenLite.to(header51, 1, {background:'#ffffff',opacity:1}); 

  }); 
  header61.hover(function() {
    TweenLite.to(header4img1, .5, {opacity:0,y:0});
    TweenLite.to(header5img1, .5, {opacity:0,y:0});
    TweenLite.to(header6img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header71, 1, {background:'#ffffff',opacity:0.8}); 
  }, function() {
     TweenLite.to(header6img1, .5, {opacity:0,y:0});
    TweenLite.to(header61, 1, {background:'#ffffff',opacity:1}); 
  }); 
  header71.hover(function() {
    TweenLite.to(header9img1, .5, {opacity:0,y:0});
    TweenLite.to(header8img1, .5, {opacity:0,y:0});
    TweenLite.to(header7img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header81, 1, {background:'#ffffff',opacity:0.8}); 
  }, function() {
    TweenLite.to(header7img1, .5, {opacity:0,y:0});
    TweenLite.to(header71, 1, {background:'#ffffff',opacity:1}); 

  }); 
  header81.hover(function() {
    TweenLite.to(header6img1, .5, {opacity:0,y:0});
    TweenLite.to(header7img1, .5, {opacity:0,y:0});
    TweenLite.to(header8img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header91, 1, {background:'#ffffff',opacity:0.8}); 

  }, function() {
    TweenLite.to(header8img1, .5, {opacity:0,y:0});
    TweenLite.to(header51, 1, {background:'#ffffff',opacity:1}); 

  }); 
  header91.hover(function() {
    TweenLite.to(header8img1, .5, {opacity:0,y:0});
    TweenLite.to(header7img1, .5, {opacity:0,y:0});
    TweenLite.to(header9img1, .5, {opacity:1,y:50,zIndex: 0});
    TweenLite.to(header91, 1, {background:'#ffffff',opacity:0.8}); 
  }, function() {
     TweenLite.to(header9img1, .5, {opacity:0,y:0});
    TweenLite.to(header91, 1, {background:'#ffffff',opacity:1}); 
  });
// slick

  // $(".row").mousewheel(function(event, delta) {
  //       this.scrollLeft -= (delta * 30);
  //       event.preventDefault();
 //     });

 // page animation heres
   $(document).ready(function(){
  var mheader1 = $('#msection1');
  var mheader1img = $('#msection1img');
  var mheading1h1 = $('#msectionheading1');

  var mheader2 = $('#msection2');
  var mheader2img = $('#msection2img');
  var mheading1h3 = $('#msectionheading4');

  var mheader3 = $('#msection3');
  var mheader3img = $('#msection3img');
  var mheading1h4 = $('#msectionheading4');

  var mheader3 = $('#msection4');
  var mheading1h6 = $('#msectionheading9');

  var bottom =$('#headingbottom');
  var bottom1 =$('#headingbottom1');

   var tl = new TimelineLite()
    $('#pagepiling').pagepiling({

          sectionsColor: ['#fff', '#fff', '#fff', '#fff'],
          navigation: {
            'position': 'right',
            'textColor': 'grey',
            'bulletsColor': 'grey',
          },
          afterRender: function(){
            TweenLite.to(mheader1img, .5, {opacity:1,y:50,zIndex:0});
            TweenLite.to(bottom, 0, {ease: Circ.easeInOut,y:0});
          },
          onLeave: function(index, nextIndex, direction){
              // if(nextIndex == 1 && direction == 'up'){
              //    TweenLite.to(mheader1, .1, {opacity:0,background:'transparent'});
              // }
              // if (nextIndex == 2 && direction == 'up') {
              //     TweenLite.to(mheader2, .1, {opacity:0,background:'transparent'});
              //     TweenLite.to(mheader1, .1, {opacity:0,background:'transparent'});

              // }             
              // if (nextIndex == 4 && direction == 'up') {
              //      TweenLite.to(mheader3img, 1, {opacity:1,y:50,zIndex: 0});
              // }
          },
          afterLoad: function(anchorLink, index){
            //using index
            if(index == 1){
              TweenLite.to(mheader1img, .5, {opacity:1,y:50,zIndex:0});
              TweenLite.to(mheader2img, .1, {opacity:0});
              TweenLite.to(mheader3img, .1, {opacity:0});
              TweenLite.to(mheading1h3, .5, {y:300,zIndex:1});
              TweenLite.to(bottom, .8, {ease: Circ.easeInOut,y:0});  

              tl.to(bottom, .5, {ease: Circ.easeInOut,y:50});
              tl.set(bottom, {text:"Seed"});
              tl.to(bottom, .5, {ease: Circ.easeInOut,y:0});
            }
            if(index == 2){
             TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, 1, {opacity:1,y:50,zIndex: 0});
             TweenLite.to(mheader3img, .1, {opacity:0});

             tl.to(bottom, .1, {ease: Circ.easeOut,y:50});
             tl.set(bottom, {text:"Series A"});
             tl.to(bottom, .1, {ease: Circ.easeOut,y:0});
            }
            if(index == 3){
             TweenLite.to(mheader3, .2, {opacity:1,background:'#fff'});
             TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, 1, {opacity:1,y:50,zIndex: 0});

             tl.to(bottom, .5, {ease: Circ.easeInOut,y:50});
             tl.set(bottom, {text:"About"});
             tl.to(bottom, .5, {ease: Circ.easeInOut,y:0});
            }            
            if(index == 4){
            TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, .1, {opacity:0});

             tl.to(bottom, .5, {ease: Circ.easeInOut,y:50});
             tl.set(bottom, {text:"Resources"});
             tl.to(bottom, 1, {ease: Circ.easeInOut,y:0});
            }            
            if(index == 5){
            TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, .1, {opacity:0});

             tl.to(bottom, .5, {ease: Circ.easeInOut,y:50});
             tl.set(bottom, {text:"Co-invest"});
             bottom.removeClass('h5')
             bottom.addClass('h1')
             tl.to(bottom, .5, {ease: Circ.easeInOut,y:0});
            }
            if(index == 6){
              TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, .1, {opacity:0});
             tl.to(bottom, .1, {ease: Circ.easeInOut,y:0});
             tl.to(bottom, .5, {ease: Circ.easeInOut,y:50});
             tl.set(bottom, {text:"&copy;Copyrights"});
             bottom.removeClass('h1')
             bottom.addClass('h5')
             bottom.css({
               paddingBottom: '11px',
               paddingTop: '11px'
             }); 
              tl.to(bottom, .5, {ease: Circ.easeInOut,y:0});
            }
            if (index == 7) {
              TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, .1, {opacity:0});
            }
            if (index == 8) {
              TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, .1, {opacity:0});
            }
            if (index == 9) {
              TweenLite.to(mheader1img, .1, {opacity:0});
             TweenLite.to(mheader2img, .1, {opacity:0});
             TweenLite.to(mheader3img, .1, {opacity:0});
            }
          }
      });

  var icon1 = $('#mainicon');
  var icon2 = $('#facebook');
  var icon3 = $('#mail');
  var icon5 = $('#linkedin');
  var icon4 = $('#cross');
  icon1.click(function(event) {
    TweenLite.to(icon2, .4, {opacity:1,x:-5,zIndex:0,ease: Expo.easeInOut});
   TweenLite.to(icon3, .4, {opacity:1,x:-5,zIndex:0,ease: Expo.easeInOut});
   TweenLite.to(icon5, .4, {opacity:1,x:-5,zIndex:0,ease: Expo.easeInOut});
   TweenLite.to(icon4, .4, {opacity:1,x:-5,zIndex:0,ease: Expo.easeInOut});
  }); 
  icon4.click(function(event) {
    TweenLite.to(icon2, .4, {opacity:0,x:0,zIndex:0,ease: Expo.easeInOut});
   TweenLite.to(icon3, .4, {opacity:0,x:-0,zIndex:0,ease: Expo.easeInOut});
   TweenLite.to(icon5, .4, {opacity:0,x:-0,zIndex:0,ease: Expo.easeInOut});
   TweenLite.to(icon4, .4, {opacity:0,x:-0,zIndex:0,ease: Expo.easeInOut});
  });

    var scrollstep = $(window).width();
    var width = $("#width").width();
    // alert(width)
if (scrollstep <= 1024) {

  var percentage = (scrollstep / 100) * 7.81;
  var forscrol = Math.round(scrollstep - percentage);
    // var scrollstep = 1770;
      // $fn.scrollSpeed(step, speed, easing);
      jQuery.scrollSpeed(width, 600);
}

    });

})(jQuery);









