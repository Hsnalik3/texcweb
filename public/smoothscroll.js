// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            
          };
        });
      }
    }
  });
  var width = window.innerWidth
  if (width<= 520) {
    var navTop = $('#navtop').offset().top;

      $(window).scroll(function(){
          if ($(this).scrollTop() >= navTop) {
              $('#navtop').css('position', 'fixed');
              $('#navtop').css('top', '10%');
              $('#navtop').css('backgroundColor', '#fff');
              $('#navtop').css('zIndex', '111');
          } else {
              $('#navtop').css('position', 'relative');
              $('#navtop').css('top', '0%');
          }
      });
    }
else {
     var navTop2 = $('#navtop2').offset().top;

      $(window).scroll(function(){
          if ($(this).scrollTop() >= navTop2) {
              $('#navtop2').css('position', 'fixed');
              $('#navtop2').css('top', '10%');
              $('#navtop2').css('backgroundColor', '#fff');
              $('#navtop2').css('zIndex', '111');
          } else {
              $('#navtop2').css('position', 'relative');
              $('#navtop2').css('top', '0%');
          }
      });
}
